#!/bin/bash
KEY_DIR=~/rsa/keys
OUTPUT_DIR=~/client-configs/files
BASE_CONFIG=~/client-configs/base.conf
cat ${BASE_CONFIG} \
&lt;(echo -e &#39;&lt;ca&gt;&#39;) \
${KEY_DIR}/ca.crt \
&lt;(echo -e &#39;&lt;/ca&gt;\n&lt;cert&gt;&#39;) \
${KEY_DIR}/${1}.crt \
&lt;(echo -e &#39;&lt;/cert&gt;\n&lt;key&gt;&#39;) \
${KEY_DIR}/${1}.key \
&lt;(echo -e &#39;&lt;/key&gt;&#39;) \
&gt; ${OUTPUT_DIR}/${1}.ovpn